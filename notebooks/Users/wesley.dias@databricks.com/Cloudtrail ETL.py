# Databricks notebook source
# MAGIC %run /databricks/streaming.logs

# COMMAND ----------

from pyspark.sql.functions import *
from pyspark.sql.streaming import ProcessingTime
from pyspark.sql.types import *
from datetime import datetime

# COMMAND ---------------------------------

cloudTrailSchema = StructType() \
  .add("additionalEventData", StringType()) \
  .add("apiVersion", StringType()) \
  .add("awsRegion", StringType()) \
  .add("errorCode", StringType()) \
  .add("errorMessage", StringType()) \
  .add("eventID", StringType()) \
  .add("eventName", StringType()) \
  .add("eventSource", StringType()) \
  .add("eventTime", StringType()) \
  .add("eventType", StringType()) \
  .add("eventVersion", StringType()) \
  .add("readOnly", BooleanType()) \
  .add("recipientAccountId", StringType()) \
  .add("requestID", StringType()) \
  .add("requestParameters", MapType(StringType(), StringType())) \
  .add("resources", ArrayType(StructType() \
    .add("ARN", StringType()) \
    .add("accountId", StringType()) \
    .add("type", StringType()) \
  )) \
  .add("responseElements", MapType(StringType(), StringType())) \
  .add("sharedEventID", StringType()) \
  .add("sourceIPAddress", StringType()) \
  .add("serviceEventDetails", MapType(StringType(), StringType())) \
  .add("userAgent", StringType()) \
  .add("userIdentity", StructType() \
    .add("accessKeyId", StringType()) \
    .add("accountId", StringType()) \
    .add("arn", StringType()) \
    .add("invokedBy", StringType()) \
    .add("principalId", StringType()) \
    .add("sessionContext", StructType() \
      .add("attributes", StructType() \
        .add("creationDate", StringType()) \
        .add("mfaAuthenticated", StringType()) \
      ) \
      .add("sessionIssuer", StructType() \
        .add("accountId", StringType()) \
        .add("arn", StringType()) \
        .add("principalId", StringType()) \
        .add("type", StringType()) \
        .add("userName", StringType()) \
      )
    ) \
    .add("type", StringType()) \
    .add("userName", StringType()) \
    .add("webIdFederationData", StructType() \
      .add("federatedProvider", StringType()) \
      .add("attributes", MapType(StringType(), StringType())) \
    )
  ) \
  .add("vpcEndpointId", StringType())

# COMMAND ----------

processingTime = udf(lambda: datetime.now(), TimestampType())

# COMMAND ----------

sql("SET spark.sql.files.maxPartitionBytes=%s" % (10 * 1024 * 1024))

cloudTrailS3toParquet = spark.readStream \
  .option("maxFilesPerTrigger", "2000") \
  .schema(StructType().add("Records", ArrayType(StringType()))) \
  .json("/mnt/cloudtrail/AWSLogs/*/CloudTrail/*/2017/*/*") \
  .select(
    input_file_name().alias('filename'),
    posexplode("records").alias("line", "recordJson")) \
  .withColumn("record", from_json("recordJson", cloudTrailSchema)) \
  .select(
    unix_timestamp("record.eventTime", "yyyy-MM-dd'T'hh:mm:ss").cast("timestamp").alias('timestamp'),
    "record.*",
    "filename",
    "line",
    "recordJson",
    struct(
      processingTime().alias('ingestTime')).alias('timing')) \
  .withWatermark("timestamp", "1 year") \
  .withColumn("date", col("timestamp").cast("date")) \
  .writeStream \
  .format("parquet") \
  .partitionBy("date") \
  .option("checkpointLocation", "/home/michael/streaming/cloudtrail.checkpoint") \
  .option("path", "/home/michael/streaming/cloudtrail") \
  .start(trigger = "5 minutes", queryName = 'cloudTrailS3toParquet')

# COMMAND ----------

# MAGIC %scala
# MAGIC sql(s"SET spark.sql.files.maxPartitionBytes=${10 * 1024 * 1024}")
# MAGIC 
# MAGIC // Reads json files for 2016-12/*, exploding the records array, writing each to kafka.  The key for each record is <fileName>:<record#>, which can be used for deduplication downstream.
# MAGIC val cloudTrailS3toKafka = spark.readStream
# MAGIC   .option("maxFilesPerTrigger", "1000")
# MAGIC   .schema(new StructType().add("Records", ArrayType(StringType)))
# MAGIC   .json("/mnt/cloudtrail/AWSLogs/*/CloudTrail/*/2017/*/*")
# MAGIC   .select(
# MAGIC     input_file_name() as 'fileName, 
# MAGIC     posexplode($"records") as ("line" :: "record" :: Nil))
# MAGIC   .select(
# MAGIC     concat($"fileName", lit(":"), $"line").as[String],
# MAGIC     $"record".as[String])
# MAGIC   .repartition(3) // Don't overload kafka...
# MAGIC   .writeStream
# MAGIC   .queryName("cloudTrailS3toKafka")
# MAGIC   .option("checkpointLocation", "/home/michael/streaming/cloudtrail-s3.checkpoint")
# MAGIC   .trigger(org.apache.spark.sql.streaming.ProcessingTime("5 minute"))
# MAGIC   .foreach(new com.databricks.streaming.KafkaSink("cloudtrail", dedup = false))
# MAGIC   .start()

# COMMAND ----------

